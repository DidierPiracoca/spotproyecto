package com.example.aplicacionspot.model.interfaz;

import com.example.aplicacionspot.model.entities.Spot;
import com.example.aplicacionspot.model.entities.Usuario;

public interface UploadCallbackSpot {
    void onUploadCompleteUser(Usuario result);
    void onUploadCompleteSpot(Spot spot);
}
