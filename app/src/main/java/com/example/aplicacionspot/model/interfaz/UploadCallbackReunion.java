package com.example.aplicacionspot.model.interfaz;

import com.example.aplicacionspot.model.entities.Reunion;

public interface UploadCallbackReunion {
    void onUploadComplete(Reunion result);
}
