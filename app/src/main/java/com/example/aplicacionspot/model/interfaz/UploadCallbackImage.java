package com.example.aplicacionspot.model.interfaz;

import com.example.aplicacionspot.model.entities.ImagenSpot;

public interface UploadCallbackImage {

    void onUploadComplete(ImagenSpot result);
}
