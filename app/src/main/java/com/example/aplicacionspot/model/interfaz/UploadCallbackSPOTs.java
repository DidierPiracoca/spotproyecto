package com.example.aplicacionspot.model.interfaz;


import com.example.aplicacionspot.model.entities.Spot;

import java.util.ArrayList;

public interface UploadCallbackSPOTs {
    void onUploadComplete(ArrayList<Spot> result);
}