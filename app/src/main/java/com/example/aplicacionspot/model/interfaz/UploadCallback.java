package com.example.aplicacionspot.model.interfaz;



public interface UploadCallback  {
    void onUploadComplete(boolean result);
}