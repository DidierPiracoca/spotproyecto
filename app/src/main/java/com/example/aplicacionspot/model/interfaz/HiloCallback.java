package com.example.aplicacionspot.model.interfaz;

public interface HiloCallback {
    void onHiloFinalizado();
}