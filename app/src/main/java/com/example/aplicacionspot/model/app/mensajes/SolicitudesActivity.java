package com.example.aplicacionspot.model.app.mensajes;

import static com.example.aplicacionspot.model.utils.InfoGeneral.usuario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
/** @hide */
import com.example.aplicacionspot.R;
import com.example.aplicacionspot.model.entities.Amistad;
import com.example.aplicacionspot.model.firebase.database.DomainUser;
import com.example.aplicacionspot.model.utils.AdapterSolicitudes;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class SolicitudesActivity extends AppCompatActivity {

    public ArrayList<Amistad> solicitudes= new ArrayList<>();
    public AdapterSolicitudes adapter;
    RecyclerView lista;
    ImageView close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitudes);
        close=findViewById(R.id.closeSolicitud);
        lista=findViewById(R.id.notificaciones_list);
        close.setOnClickListener(v->finish());
        LinearLayoutManager layoutManagerReunion = new LinearLayoutManager(this.getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        lista.setLayoutManager(layoutManagerReunion);
        adapter = new AdapterSolicitudes(this, solicitudes);
        lista.setAdapter(adapter);
        DomainUser domainUser= new DomainUser();
        domainUser.getAmistades(FirebaseDatabase.getInstance(),usuario.getId(),solicitudes,adapter);


    }
}